import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.11

Window {
    id: window
    width: 400
    height: 600
    visible: true
    property alias z_slider: z_slider
    property alias y_slider: y_slider
    property alias x_slider: x_slider

    property alias comboBox: comboBox

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        spacing: 1

        RowLayout {
            id: rowLayout
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.fillHeight: false

            Label {
                id: hipLabel
                text: qsTr("hip")
            }

            Slider {
                id: hip
                stepSize: 0.05
                to: 0.8
                from: -0.8
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                value: 0
                onValueChanged: controller.hip = value

            }

            Label {
                id: label
                text: hip.value.toFixed(2)
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
        }

        RowLayout {
            id: rowLayout1
            width: 100
            height: 100
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Label {
                id: thighLabel
                text: qsTr("thigh")
            }

            Slider {
                id: thigh
                stepSize: 0.05
                to: 4.18
                from: -1.04
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                value: 0
                onValueChanged: controller.thigh = value
            }

            Label {
                id: label1
                text: thigh.value.toFixed(2)
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
        }

        RowLayout {
            id: rowLayout2
            width: 100
            height: 100
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Label {
                id: calfLabel
                text: qsTr("calf")
            }

            Slider {
                id: calf
                stepSize: 0.05
                to: -0.9
                from: -2.68
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                value: -1
                onValueChanged: controller.calf = value
            }

            Label {
                id: label2
                text: calf.value.toFixed(2)
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
        }

        RowLayout {
            id: rowLayout3
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Label {
                id: hipLabel1
                text: qsTr("X")
            }

            Slider {
                id: x_slider
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                stepSize: 0.005
                onValueChanged: controller.to_x = value
                value: -0.05
                to: 0.30
                from: -0.30
            }

            Label {
                id: label3
                text: x_slider.value.toFixed(2)
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
            Layout.minimumHeight: 40
            Layout.minimumWidth: 265
            Layout.fillHeight: false
        }


        RowLayout {
            id: rowLayout4
            width: 100
            height: 100
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Label {
                id: thighLabel1
                text: qsTr("Y")
            }

            Slider {
                id: y_slider
                stepSize: 0.001
                onValueChanged: controller.to_y = value
                value: 0.084
                to: 0.2
                from: -0.2
            }

            Label {
                id: label4
                text: y_slider.value.toFixed(2)
            }
            Layout.fillHeight: false
        }

        RowLayout {
            id: rowLayout5
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Label {
                id: calfLabel1
                text: qsTr("Z")
            }

            Slider {
                id: z_slider
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                stepSize: 0.005
                onValueChanged: controller.to_z = value
                value: 0.25
                to: 0.4
                from: 0.0
            }

            Label {
                id: label5
                text: z_slider.value.toFixed(2)
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
            Layout.fillHeight: true
        }

        RowLayout {
            id: rowLayout6
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Label {
                id: yrotate
                text: qsTr("Y angle")
            }

            Slider {
                id: z_slider1
                stepSize: 0.005
                onValueChanged: controller.angle = value
                value: 0
                to: 3.14
                from: -3.14
            }

            Label {
                id: label6
                text: z_slider1.value.toFixed(2)
            }
            Layout.fillHeight: true
        }





        ComboBox {
            id: comboBox
            displayText: "Вид управления"
            Layout.minimumWidth: 200
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            model: ["Обобщенные координаты", "По ОЗК", "Используя IMU"]
//            onModelChanged: controller.setMethod(currentText)
            onActivated: controller.setMethod(currentText)
        }
        Connections {
            target: comboBox
            function onCurrentTextChanged () {
                if (comboBox.currentText == "По ОЗК") {
                    controller.to_x = x_slider.value
                    controller.to_y = y_slider.value
                    controller.to_z = z_slider.value
                }

            }
        }
    }

    Timer {
        interval: 10; running: true; repeat: true
        onTriggered: controller.spin()
    }
    Shortcut {
        sequence: StandardKey.Quit
        context: Qt.ApplicationShortcut
        onActivated: Qt.quit()
    }



}

/*##^##
Designer {
    D{i:0;height:600;width:450}
}
##^##*/
