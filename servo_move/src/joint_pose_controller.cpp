#include <servo_move/joint_pose_controller.h>

JointController::JointController(std::string robot_name, QObject* parent)
  : QObject (parent)
  , nh_()
  , q0_(0.0f)
  , q1_(0.0f)
  , q2_(0.0f)
  , x_(0.197f)
  , y_(0.084f)
  , z_(0.03f)
  , pubs_()
  , subs_()
{
  std::vector<std::string> joint_names({"hip", "thigh", "calf"});
  std::vector<std::string> leg_names({"FR", "FL", "RL", "RR"});

  joint_states_.resize(12);

  std::vector<std::string> prefix_vector;
  for (size_t leg = 0; leg < 4; leg++)
  {
    for (size_t joint = 0; joint < 3; joint++)
    {
      static uint8_t counter = 0;
      prefix_vector.push_back("/" + robot_name + "_gazebo/" + leg_names.at(leg) + "_" +
                         joint_names.at(joint) + "_controller/");

      pubs_.push_back(nh_.advertise<unitree_legged_msgs::MotorCmd>(
                        prefix_vector.back() + "command", 1) );

    }
  }
  subs_.push_back(nh_.subscribe(prefix_vector.at(0) + "state", 1, &JointController::jointCallbackFRHip, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(1) + "state", 1, &JointController::jointCallbackFRThigh, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(2) + "state", 1, &JointController::jointCallbackFRCalf, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(3) + "state", 1, &JointController::jointCallbackFLHip, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(4) + "state", 1, &JointController::jointCallbackFLThigh, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(5) + "state", 1, &JointController::jointCallbackFLCalf, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(6) + "state", 1, &JointController::jointCallbackRLHip, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(7) + "state", 1, &JointController::jointCallbackRLThigh, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(8) + "state", 1, &JointController::jointCallbackRLCalf, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(9) + "state", 1, &JointController::jointCallbackRRHip, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(10) + "state", 1, &JointController::jointCallbackRRThigh, this));
  subs_.push_back(nh_.subscribe(prefix_vector.at(11) + "state", 1, &JointController::jointCallbackRCalf, this));
  imu_sub_ = nh_.subscribe("/trunk_imu", 10, &JointController::imuCallback, this);
}

JointController::~JointController()
{

}

void JointController::setMethod(QString msg)
{
  control_method_ = msg;
}

void JointController::imuCallback(const sensor_msgs::ImuPtr& msg)
{
  imu_msg_ = *msg;
}

void JointController::jointCallbackFRHip(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(0) = msg->q;}

void JointController::jointCallbackFRThigh(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(1) = msg->q;}

void JointController::jointCallbackFRCalf(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(2) = msg->q;}

void JointController::jointCallbackFLHip(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(3) = msg->q;}

void JointController::jointCallbackFLThigh(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(4) = msg->q;}

void JointController::jointCallbackFLCalf(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(5) = msg->q;}

void JointController::jointCallbackRLHip(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(6) = msg->q;}

void JointController::jointCallbackRLThigh(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(7) = msg->q;}

void JointController::jointCallbackRLCalf(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(8) = msg->q;}

void JointController::jointCallbackRRHip(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(9) = msg->q;}

void JointController::jointCallbackRRThigh(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(10) = msg->q;}

void JointController::jointCallbackRCalf(const unitree_legged_msgs::MotorStatePtr &msg)
{ joint_states_.at(11) = msg->q;}

void JointController::balance()
{
  unitree_legged_msgs::MotorCmd msg;
  msg.mode = 10;
  msg.dq = 0;
  msg.tau = 0;
  msg.Kp = 300.0f;
  msg.Kd = 15.0f;

  Position stand(-0.05, 0.08, 0.3); // стойка

  tf::Quaternion qua;
  tf::quaternionMsgToTF(imu_msg_.orientation, qua);
  tf::Matrix3x3 m(qua);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);

  qDebug() << pitch;

  Position front_target = rotateTargetPointY(stand, Position(0.1805, -0.047,0), -pitch);
  Position back_target = rotateTargetPointY(stand, Position(-0.1805, -0.047,0), -pitch);


  JointConfiguration front = inverseKinematics(front_target,
                                           {joint_states_.at(0),
                                            joint_states_.at(1),
                                            joint_states_.at(2),
                                           });

  JointConfiguration back = inverseKinematics(back_target,
                                           {joint_states_.at(6),
                                            joint_states_.at(7),
                                            joint_states_.at(8),
                                           });

  msg.q = front.q1;
  pubs_.at(0).publish(msg);
  pubs_.at(3).publish(msg);
  msg.q = back.q1;
  pubs_.at(6).publish(msg);
  pubs_.at(9).publish(msg);
  msg.q = front.q2;
  pubs_.at(1).publish(msg);
  pubs_.at(4).publish(msg);
  msg.q = back.q2;
  pubs_.at(7).publish(msg);
  pubs_.at(10).publish(msg);
  msg.q = front.q3;
  pubs_.at(2).publish(msg);
  pubs_.at(5).publish(msg);
  msg.q = back.q3;
  pubs_.at(8).publish(msg);
  pubs_.at(11).publish(msg);
}


void JointController::manualIK()
{
  unitree_legged_msgs::MotorCmd msg;
  msg.mode = 10;
  msg.dq = 0;
  msg.tau = 0;
  msg.Kp = 300.0f;
  msg.Kd = 15.0f;

  Position stand(-0.05, 0.08, 0.3); // стойка

  // временно вкидываю сюда значения с ползунков (нет)
  Position p = {x_, y_, z_};

  JointConfiguration q = inverseKinematics(p,
                                           {joint_states_.at(0),
                                            joint_states_.at(1),
                                            joint_states_.at(2),
                                           });

  msg.q = q.q1;
  pubs_.at(0).publish(msg);
  pubs_.at(3).publish(msg);

  pubs_.at(6).publish(msg);
  pubs_.at(9).publish(msg);
  msg.q = q.q2;
  pubs_.at(1).publish(msg);
  pubs_.at(4).publish(msg);

  pubs_.at(7).publish(msg);
  pubs_.at(10).publish(msg);
  msg.q = q.q3;
  pubs_.at(2).publish(msg);
  pubs_.at(5).publish(msg);

  pubs_.at(8).publish(msg);
  pubs_.at(11).publish(msg);
}

void JointController::legMove()
{
  unitree_legged_msgs::MotorCmd msg;
  msg.mode = 10;
  msg.dq = 0;
  msg.tau = 0;
  msg.Kp = 300.0f;
  msg.Kd = 15.0f;


  msg.q = q0_;
  pubs_.at(0).publish(msg);
  msg.q = q1_;
  pubs_.at(1).publish(msg);
  msg.q = q2_;
  pubs_.at(2).publish(msg);
}

void JointController::spin()
{
  ros::spinOnce();
  if (control_method_ == "Обобщенные координаты")
    legMove();
  if (control_method_ == "По ОЗК")
    manualIK();
  if (control_method_ == "Используя IMU")
    balance();
}


Position rotateTargetPointY(Position target, Position transform_to_base, double angle)
{
  translation(target, transform_to_base);
  rotateMatrixY(target, angle);
  translation(target, Position(-transform_to_base.x, -transform_to_base.y, -transform_to_base.z));
  return target;
}

Position rotateTargetPointX(Position target, Position transform_to_base, double angle)
{
  translation(target, transform_to_base);
  rotateMatrixX(target, angle);
  translation(target, Position(-transform_to_base.x, -transform_to_base.y, -transform_to_base.z));
  return target;
}
