#pragma once

#include <ros/ros.h>
#include <QObject>
#include <unitree_legged_msgs/MotorCmd.h>
#include <unitree_legged_msgs/MotorState.h>
#include <inverse_kinematics/src/main/inverse_kinematics.h>
#include <sensor_msgs/Imu.h>
#include <tf2/impl/utils.h>
#include <tf/transform_datatypes.h>

#include <QDebug>



class JointController : public QObject
{
  Q_OBJECT
  Q_PROPERTY(float calf MEMBER q2_ NOTIFY calfChanged)
  Q_PROPERTY(float thigh MEMBER q1_ NOTIFY thighChanged)
  Q_PROPERTY(float hip MEMBER q0_ NOTIFY hipChanged)
  Q_PROPERTY(float to_x MEMBER x_ NOTIFY xChanged)
  Q_PROPERTY(float to_y MEMBER y_ NOTIFY yChanged)
  Q_PROPERTY(float to_z MEMBER z_ NOTIFY zChanged)
  Q_PROPERTY(float angle MEMBER y_angle_ NOTIFY angleChanged)
public:
  explicit JointController(std::string robot_name, QObject* parent = nullptr);
  ~JointController();

  void jointCallbackFRHip(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackFRThigh(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackFRCalf(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackFLHip(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackFLThigh(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackFLCalf(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackRLHip(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackRLThigh(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackRLCalf(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackRRHip(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackRRThigh(const unitree_legged_msgs::MotorStatePtr& msg);
  void jointCallbackRCalf(const unitree_legged_msgs::MotorStatePtr& msg);

  void imuCallback(const sensor_msgs::ImuPtr& msg);

  void balance();
  void manualIK();
  void legMove();



signals:
  void calfChanged(float);
  void thighChanged(float);
  void hipChanged(float);
  void xChanged(float);
  void yChanged(float);
  void zChanged(float);
  void angleChanged(double);

public slots:
  void spin();
  void setMethod(QString);

private:
  ros::NodeHandle nh_;
  float q0_;
  float q1_;
  float q2_;
  float x_;
  float y_;
  float z_;
  double y_angle_;
  std::vector<ros::Publisher> pubs_;
  std::vector<ros::Subscriber> subs_;
  ros::Subscriber imu_sub_;
  std::vector<float> joint_states_;
  std::vector<std::function<void(const unitree_legged_msgs::MotorStatePtr&)>> callbacks_;
  sensor_msgs::Imu imu_msg_;
  QString control_method_;
};

Position rotateTargetPointY(Position target, Position transform_to_base, double angle);
Position rotateTargetPointX(Position target, Position transform_to_base, double angle);

