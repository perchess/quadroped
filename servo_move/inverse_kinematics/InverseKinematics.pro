TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/main/inverse_kinematics.cpp \
    src/main/main.cpp

HEADERS += \
    src/main/inverse_kinematics.h
